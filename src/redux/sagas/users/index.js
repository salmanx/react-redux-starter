import {takeLatest, put, call} from 'redux-saga/effects';
import AxiosServices from '../../../networks/AxiosService';
import ApiServices from '../../../networks/ApiServices';
import {USERS} from '../../constants/users';

function* usersSagas(actions) {
  try {
    const result = yield call(
      AxiosServices.get,
      ApiServices.USERS_API,
      actions.body,
      true
    ); // true when local server
    yield put({type: USERS.SUCCESS, result: result.data});
  } catch (err) {
    yield put({type: USERS.FAILURE, error: 'something went wrong'});
  }
}

export default function* usersagas() {
  yield takeLatest(USERS.MAIN, usersSagas);
}
