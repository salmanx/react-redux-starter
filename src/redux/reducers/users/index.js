import {USERS} from '../../constants/users';

const initialState = {
  users: [],
  loading: null
};

export default function usersReducer(state = initialState, action) {
  switch (action.type) {
    case USERS.MAIN:
      return {
        ...state,
        users: state.users,
        loading: true
      };

    case USERS.SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.result
      };

    case USERS.FAILURE:
      return {
        ...state,
        loading: null,
        users: []
      };

    default:
      return state;
  }
}
