import {USERS} from '../../constants/users';

export function getAllUsers(body = {}) {
  return {
    type: USERS.MAIN,
    body
  };
}
