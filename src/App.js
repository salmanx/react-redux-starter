import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';

// load Components
import Header from './components/layouts/header/Header';
import Home from './components/home/Home';
import About from './components/home/About';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <>
          <Header />
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/about" component={About} exact />
          </Switch>
          {/* <Footer /> */}
        </>
      </BrowserRouter>
    );
  }
}

export default App;
