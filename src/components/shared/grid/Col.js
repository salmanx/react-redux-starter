import styled, {css} from 'styled-components';

const Col = styled.div`
  flex: ${(props) => props.size};
  margin: ${(props) => props.margin};
  margin-left: ${(props) => props.ml};
  margin-right: ${(props) => props.mr};
  margin-top: ${(props) => props.mt};
  margin-bottom: ${(props) => props.mb};
  padding: ${(props) => props.padding};
  padding-left: ${(props) => props.pl};
  padding-right: ${(props) => props.pr};
  padding-top: ${(props) => props.pt};
  padding-bottom: ${(props) => props.pb};

  ${(props) =>
    props &&
    props['hidden-xs'] &&
    css`
      @media only screen and (max-width: 480px) {
        display: none;
      }
    `};

  ${(props) =>
    props &&
    props['hidden-sm'] &&
    css`
      @media only screen and (max-width: 575px) {
        display: none;
      }
    `};
`;

export default Col;
