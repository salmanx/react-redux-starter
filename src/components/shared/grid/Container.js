import styled, {css} from 'styled-components';

const Container = styled.div`
  margin: ${(props) => (props.margin ? props.margin : '0 auto')};
  padding: ${(props) => (props.padding ? props.padding : '')};
  width: ${(props) => (props.width ? props.width : '1080px')};
  height: ${(props) => (props.height ? props.height : '')};
  display: block;

  ${(props) =>
    props &&
    props['flex-container'] &&
    css`
      display: flex;
      justify-content: space-around;
    `};
`;

export default Container;
