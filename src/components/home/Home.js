import React, {Component} from 'react';

import {connect} from 'react-redux';
import {getAllUsers} from '../../redux/actions/users';

class Home extends Component {
  componentDidMount() {
    this.props.getAllUsers();
  }

  render() {
    return <h1>Home</h1>;
  }
}

function mapStateToProps(state) {
  return {
    usersReducers: state.usersReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllUsers: () => dispatch(getAllUsers())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
