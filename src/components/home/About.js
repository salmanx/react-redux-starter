import React, {Component} from 'react';

import {connect} from 'react-redux';

class About extends Component {
  componentDidMount() {
    // console.log(this.props.usersReducers.users);
  }

  render() {
    return <h1>About</h1>;
  }
}

function mapStateToProps(state) {
  return {
    usersReducers: state.usersReducer
  };
}

export default connect(mapStateToProps, null)(About);
