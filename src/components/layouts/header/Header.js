import React from 'react';
import MenuBar from './MenuBar';
import NavBar from './NavBar';

export default function Header() {
  return (
    <>
      <MenuBar />
      <NavBar />
    </>
  );
}
