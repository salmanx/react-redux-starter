import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';
import {Container} from '../../shared/grid/Index';

// import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
// import {faCoffee} from '@fortawesome/free-solid-svg-icons';

const useStyles = makeStyles((theme) => ({
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
    background: '#fff',
    minHeight: '70px'
  },
  toolbarLink: {
    padding: theme.spacing(1),
    fontWeight: 'bold',
    fontSize: '14px',
    flexShrink: 0,
    '&:hover': {
      textDecoration: 'none',
      borderBottom: '3px solid #666'
    }
  },
  linkIcon: {
    height: '20px',
    width: '20px',
    background: '#999',
    display: 'inline-block',
    verticalAlign: 'middle',
    borderRadius: '50%',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  }
}));

export default function NavBar() {
  const classes = useStyles();

  const sections = [
    {title: 'Technology', url: '#', icon: ''},
    {title: 'Design', url: '#', icon: ''},
    {title: 'Culture', url: '#', icon: ''},
    {title: 'Business', url: '#', icon: ''},
    {title: 'Politics', url: '#', icon: ''},
    {title: 'Opinion', url: '#', icon: ''},
    {title: 'Politic', url: '#', icon: ''},
    {title: 'Opinions', url: '#', icon: ''}
  ];

  return (
    <>
      <Toolbar component="nav" variant="dense" className={classes.toolbarSecondary}>
        <Container flex-container>
          {sections.map((section) => (
            <div key={section.title}>
              <div className={classes.linkIcon}>
                {/* <FontAwesomeIcon icon={faCoffee} /> */}
              </div>
              <Link
                color="inherit"
                noWrap
                key={section.title}
                variant="body2"
                href={section.url}
                className={classes.toolbarLink}
              >
                {section.title}
              </Link>
            </div>
          ))}
        </Container>
      </Toolbar>
    </>
  );
}
